package emergencias;

import emergencias.excepciones.ExcepcionLogica;
import emergencias.logica.model.AmbHospital;
import emergencias.logica.model.AmbPrivada;
import emergencias.logica.model.Hospital;
import emergencias.logica.model.Paciente;
import emergencias.logica.service.ServicioEmergencias;

/**
 * Created by Mario on 29/10/2015.
 */
public class PopulateDatabase {
    public static void main(String[] args) {
        ServicioEmergencias se = ServicioEmergencias.dameServicioEmergencias();

        //Creamos algunos hospitales y ambulancias
        Hospital h1 = new Hospital("Hospital Costa del Azafran", "Avenida de la Paella, 1", 45.0, 15.0);
        Hospital h2 = new Hospital("Psiquiatrico Los Pitufos", "Calle del Limbo, 33", 46.0, 14.0);
        AmbPrivada a1 = new AmbPrivada("12345 KIS", "Azul", 44.0, 13.9, "Sanitatis Privatis SA");
        AmbHospital a2 = new AmbHospital("6789 DRY", "Rojo", 37.0, 14.5, h1);
        // y los a�adimos a las colecciones respectivas
        try {
            se.anyadirHospital(h1);
        } catch (ExcepcionLogica excepcionLogica) {
            excepcionLogica.printStackTrace();
        }
        try {
            se.anyadirHospital(h2);
        } catch (ExcepcionLogica excepcionLogica) {
            excepcionLogica.printStackTrace();
        }
        try {
            se.anyadirAmbulancia(a1);
        } catch (ExcepcionLogica excepcionLogica) {
            excepcionLogica.printStackTrace();
        }
        try {
            se.anyadirAmbulancia(a2);
        } catch (ExcepcionLogica excepcionLogica) {
            excepcionLogica.printStackTrace();
        }
        // Los listamos
        System.out.println(se.listarHospitales());
        System.out.println(se.listarAmbulancias());

        // creamos algunos pacientes
        Paciente p1 = new Paciente("10123456A", "Juan", "Martinez Gandia", "Calle Santiago, 4 Valencia",
                123453250, 50, 'H');
        Paciente p2 = new Paciente("10123457A", "Pedro", "Suecaz Santos", "Calle San Vicente, 4 Valencia",
                123453251, 25, 'H');
        Paciente p3 = new Paciente("10123458A", "Ana", "Bezo Tosa", "Calle Francia, 4 Valencia", 923453251, 34, 'M');
//        se.eliminarPaciente(p1);
//        se.eliminarPaciente(p2);
//        se.eliminarPaciente(p3);
        try {
            se.altaPaciente(p1);
        } catch (ExcepcionLogica excepcionLogica) {
            excepcionLogica.printStackTrace();
        }
        try {
            se.altaPaciente(p2);
        } catch (ExcepcionLogica excepcionLogica) {
            excepcionLogica.printStackTrace();
        }
        try {
            se.altaPaciente(p3);
        } catch (ExcepcionLogica excepcionLogica) {
            excepcionLogica.printStackTrace();
        }
    }
}


