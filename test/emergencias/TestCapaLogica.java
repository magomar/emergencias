package emergencias;

import emergencias.excepciones.ExcepcionLogica;
import emergencias.logica.model.Paciente;
import emergencias.logica.service.ServicioEmergencias;

public class TestCapaLogica {

    public static void main(String[] args) {

        // Se crea el Servicio de Emergencias
        ServicioEmergencias se = ServicioEmergencias.dameServicioEmergencias();

        // Creamos un paciente
        Paciente p1 = new Paciente("01010101H", "Binario", "On Off", "Plaza Digital, 2 Valencia",
                63453559, 16, 'H');

        System.out.println("Buscamos un paciente que no existe");
        if (se.buscarPaciente(p1.getDni()) == null)
            System.out.println("El paciente con DNI " + p1.getDni() + " no existe");

        System.out.println("Ahora lo intentamos eliminar");
        try {
            se.eliminarPaciente(p1);
        } catch (ExcepcionLogica excepcionLogica) {
            excepcionLogica.printStackTrace();
        }
        System.out.println("Lo damos de alta");
        try {
            se.altaPaciente(p1);
        } catch (ExcepcionLogica excepcionLogica) {
            excepcionLogica.printStackTrace();
        }
        System.out.println("Lo intentamos dar de alta nuevamente");//
        try {
            se.altaPaciente(p1);
        } catch (ExcepcionLogica excepcionLogica) {
            excepcionLogica.printStackTrace();
        }
        System.out.println("Lo eliminamos");
        try {
            se.eliminarPaciente(p1);
        } catch (ExcepcionLogica excepcionLogica) {
            excepcionLogica.printStackTrace();
        }
    }
}
