package emergencias.persistencia.dto;

public class AmbPrivadaDTO extends AmbulanciaDTO {

    private String companyia;

    public AmbPrivadaDTO(String numRegistro, String equipo, Double latitud, Double longitud, String companyia) {
        super(numRegistro, equipo, latitud, longitud);
        this.companyia = companyia;
    }

    public String getCompanyia() {
        return companyia;
    }

    public void setCompanyia(String companyia) {
        this.companyia = companyia;
    }
}