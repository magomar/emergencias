package emergencias.persistencia.dto;

/**
 * Created by Mario on 28/10/2015.
 */
public class AtiendeDTO {
    private String idHospital;
    private String idEspecialidad;

    public AtiendeDTO(String idHospital, String idEspecialidad) {
        this.idHospital = idHospital;
        this.idEspecialidad = idEspecialidad;
    }

    public String getIdHospital() {
        return idHospital;
    }

    public void setIdHospital(String idHospital) {
        this.idHospital = idHospital;
    }

    public String getIdEspecialidad() {
        return idEspecialidad;
    }

    public void setIdEspecialidad(String idEspecialidad) {
        this.idEspecialidad = idEspecialidad;
    }
}
