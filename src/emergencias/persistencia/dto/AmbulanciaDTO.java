package emergencias.persistencia.dto;

public abstract class AmbulanciaDTO {
    private String numRegistro;
    private String equipo;
    private Double latitud;
    private Double longitud;

    public AmbulanciaDTO(String numRegistro, String equipo, Double latitud, Double longitud) {
        this.numRegistro = numRegistro;
        this.equipo = equipo;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public String getNumRegistro() {
        return numRegistro;
    }

    public void setNumRegistro(String numRegistro) {
        this.numRegistro = numRegistro;
    }

    public String getEquipo() {
        return equipo;
    }

    public void setEquipo(String equipo) {
        this.equipo = equipo;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

}
