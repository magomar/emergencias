package emergencias.persistencia.dto;

public class AmbHospitalDTO extends AmbulanciaDTO {
    private String idHospital;

    public AmbHospitalDTO(String numRegistro, String equipo, Double latitud, Double longitud, String idHospital) {
        super(numRegistro, equipo, latitud, longitud);
        this.idHospital = idHospital;
    }

    public String getIdHospital() {
        return idHospital;
    }

    public void setIdHospital(String idHospital) {
        this.idHospital = idHospital;
    }
}
