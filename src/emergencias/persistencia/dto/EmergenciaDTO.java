package emergencias.persistencia.dto;

public class EmergenciaDTO {

    private String idEmergencia;
    private Double latitud;
    private Double longitud;
    private String fecha;
    private String hora;
    private String dniPaciente;
    private String idAmbulancia;
    private String idHospital;
    private String idEspecialidad;

    public EmergenciaDTO(String idEmergencia, Double latitud, Double longitud, String fecha, String hora, String dniPaciente, String idAmbulancia, String idHospital, String idEspecialidad) {
        this.idEmergencia = idEmergencia;
        this.latitud = latitud;
        this.longitud = longitud;
        this.fecha = fecha;
        this.hora = hora;
        this.dniPaciente = dniPaciente;
        this.idAmbulancia = idAmbulancia;
        this.idHospital = idHospital;
        this.idEspecialidad = idEspecialidad;
    }

    public String getIdEmergencia() {
        return idEmergencia;
    }

    public void setIdEmergencia(String idEmergencia) {
        this.idEmergencia = idEmergencia;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getIdAmbulancia() {
        return idAmbulancia;
    }

    public void setIdAmbulancia(String idAmbulancia) {
        this.idAmbulancia = idAmbulancia;
    }

    public String getDniPaciente() {
        return dniPaciente;
    }

    public void setDniPaciente(String dniPaciente) {
        this.dniPaciente = dniPaciente;
    }

    public String getIdHospital() {
        return idHospital;
    }

    public void setIdHospital(String idHospital) {
        this.idHospital = idHospital;
    }

    public String getIdEspecialidad() {
        return idEspecialidad;
    }

    public void setIdEspecialidad(String idEspecialidad) {
        this.idEspecialidad = idEspecialidad;
    }
}
