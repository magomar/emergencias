package emergencias.persistencia.dao;

import emergencias.excepciones.ExcepcionDAO;
import emergencias.persistencia.dto.AmbHospitalDTO;
import emergencias.persistencia.dto.AmbPrivadaDTO;
import emergencias.persistencia.dto.AmbulanciaDTO;

import java.util.List;

public interface AmbulanciaDAO {
    AmbulanciaDTO buscarAmbulancia(String numRegistro) throws ExcepcionDAO;

    void crearAmbulancia(AmbPrivadaDTO ambulancia) throws ExcepcionDAO;

    void crearAmbulancia(AmbHospitalDTO ambulancia) throws ExcepcionDAO;

    List<AmbulanciaDTO> listarAmbulancias() throws ExcepcionDAO;
}
