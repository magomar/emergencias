package emergencias.persistencia.dao;

import emergencias.excepciones.ExcepcionDAO;
import emergencias.persistencia.dto.EmergenciaDTO;

import java.util.List;

public interface EmergenciaDAO {
    EmergenciaDTO buscarEmergencia(String idRegistro) throws ExcepcionDAO;

    void crearEmergencia(EmergenciaDTO emergencia) throws ExcepcionDAO;

    List<EmergenciaDTO> listarEmergencias() throws ExcepcionDAO;

}
