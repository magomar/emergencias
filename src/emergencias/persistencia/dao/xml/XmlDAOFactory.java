package emergencias.persistencia.dao.xml;

import emergencias.persistencia.dao.*;

/**
 * Created by Mario on 27/10/2015.
 */
public class XmlDAOFactory implements DAOFactory {
    // TODO implementar DAO XML
    public XmlDAOFactory(String fileName) {
    }

    @Override
    public HospitalDAO dameHospitalDAO() {
        throw new UnsupportedOperationException("DAO para XML no implementado todav�a");
    }

    @Override
    public AmbulanciaDAO dameAmbulanciaDAO() {
        throw new UnsupportedOperationException("DAO para XML no implementado todav�a");
    }

    @Override
    public PacienteDAO damePacienteDAO() {
        throw new UnsupportedOperationException("DAO para XML no implementado todav�a");
    }

    @Override
    public EspecialidadDAO dameEspecialidadDAO() {
        throw new UnsupportedOperationException("DAO para XML no implementado todav�a");
    }

    @Override
    public EmergenciaDAO dameEmergenciaDAO() {
        throw new UnsupportedOperationException("DAO para XML no implementado todav�a");
    }
}
