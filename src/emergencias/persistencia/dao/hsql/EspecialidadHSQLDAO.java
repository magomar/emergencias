package emergencias.persistencia.dao.hsql;

import emergencias.excepciones.ExcepcionDAO;
import emergencias.persistencia.dao.EspecialidadDAO;
import emergencias.persistencia.dto.EspecialidadDTO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EspecialidadHSQLDAO implements EspecialidadDAO {
    protected HSQLConnectionManager connManager;

    public EspecialidadHSQLDAO(HSQLConnectionManager connManager) throws ExcepcionDAO {
        this.connManager = connManager;
    }

    public void crearEspecialidad(EspecialidadDTO esp) throws ExcepcionDAO {
        try {
            connManager.connect();
            connManager.updateDB("insert into ESPECIALIDAD (NOMBRE) values ('" + esp.getNombre() + "')");
            connManager.close();
        } catch (Exception e) {
            throw new ExcepcionDAO(e);
        }
    }

    public List<EspecialidadDTO> listarEspecialidades() throws ExcepcionDAO {
        try {
            connManager.connect();
            ResultSet rs = connManager.queryDB("select * from ESPECIALIDAD");
            connManager.close();
            List<EspecialidadDTO> especialidades = new ArrayList<>();
            try {
                while (rs.next()) {
                    especialidades.add(buscarEspecialidad(rs.getString("NOMBRE")));
                }
                return especialidades;
            } catch (Exception e) {
                throw new ExcepcionDAO(e);
            }
        } catch (ExcepcionDAO e) {
            throw e;
        }
    }

    public List<EspecialidadDTO> listarEspecialidadesPorHospital(String nombre) throws ExcepcionDAO {
        try {
            connManager.connect();
            ResultSet rs = connManager.queryDB(
                    "select * from ESPECIALIDAD as E where exists(select * from ATIENDE as A where	E.NOMBRE = A.IDESPECIALIDAD and A.IDHOSPITAL = '"
                            + nombre + "');");
            connManager.close();
            List<EspecialidadDTO> listaEspecialidades = new ArrayList<>();

            try {
                while (rs.next()) {
                    listaEspecialidades.add(buscarEspecialidad(rs.getString("NOMBRE")));
                }
                return listaEspecialidades;
            } catch (Exception e) {
                throw new ExcepcionDAO(e);
            }
        } catch (ExcepcionDAO e) {
            throw e;
        }
    }

    public EspecialidadDTO buscarEspecialidad(String nom) throws ExcepcionDAO {
        try {
            connManager.connect();
            ResultSet rs = connManager.queryDB("select * from ESPECIALIDAD where NOMBRE= '" + nom + "';");
            connManager.close();
            if (rs.next()) {
                return new EspecialidadDTO(nom);
            } else
                return null;
        } catch (SQLException e) {
            throw new ExcepcionDAO(e);
        }
    }
}
