package emergencias.persistencia.dao.hsql;

import emergencias.excepciones.ExcepcionDAO;
import emergencias.persistencia.dao.AmbulanciaDAO;
import emergencias.persistencia.dto.AmbHospitalDTO;
import emergencias.persistencia.dto.AmbPrivadaDTO;
import emergencias.persistencia.dto.AmbulanciaDTO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AmbulanciaHSQLDAO implements AmbulanciaDAO {
    protected HSQLConnectionManager connManager;

    public AmbulanciaHSQLDAO(HSQLConnectionManager connManager) throws ExcepcionDAO {
        this.connManager = connManager;
    }

    @Override
    public void crearAmbulancia(AmbPrivadaDTO am) throws ExcepcionDAO {
        try {
            connManager.connect();
            connManager.updateDB(
                    "insert into AMBULANCIA (NUMREGISTRO,EQUIPO, LATITUD, LONGITUD) values ('" + am.getNumRegistro()
                            + "','" + am.getEquipo() + "','" + am.getLatitud() + "', '" + am.getLongitud() + "')");
            connManager.updateDB(
                    "insert into AMBPRIVADA (NUMREGISTRO, COMPANYIA) values ('" + am.getNumRegistro()
                            + "','" + am.getCompanyia() + "')");
            connManager.close();
        } catch (Exception e) {
            throw new ExcepcionDAO(e);
        }
    }

    @Override
    public void crearAmbulancia(AmbHospitalDTO am) throws ExcepcionDAO {
        try {
            connManager.connect();
            connManager.updateDB(
                    "insert into AMBULANCIA (NUMREGISTRO,EQUIPO, LATITUD, LONGITUD) values ('" + am.getNumRegistro()
                            + "','" + am.getEquipo() + "','" + am.getLatitud() + "', '" + am.getLongitud() + "')");
            connManager.updateDB(
                    "insert into AMBHOSPITAL (NUMREGISTRO, IDHOSPITAL) values ('" + am.getNumRegistro()
                            + "','" + am.getIdHospital() + "')");
            connManager.close();
        } catch (Exception e) {
            throw new ExcepcionDAO(e);
        }
    }

    @Override
    public List<AmbulanciaDTO> listarAmbulancias() throws ExcepcionDAO {
        try {
            connManager.connect();
            ResultSet rs = connManager.queryDB("select * from AMBULANCIA");
            connManager.close();
            List<AmbulanciaDTO> ambulancias = new ArrayList<>();
            try {
                while (rs.next()) {
                    ambulancias.add(buscarAmbulancia(rs.getString("NUMREGISTRO")));
                }
                return ambulancias;
            } catch (Exception e) {
                throw new ExcepcionDAO(e);
            }
        } catch (ExcepcionDAO e) {
            throw e;
        }
    }

    @Override
    public AmbulanciaDTO buscarAmbulancia(String numRegistro) throws ExcepcionDAO {
        try {
            connManager.connect();
            ResultSet rs = connManager.queryDB("select * from AMBULANCIA where NUMREGISTRO= '" + numRegistro + "'");
            ResultSet rs2 = connManager.queryDB("select * from AMBPRIVADA where NUMREGISTRO= '" + numRegistro + "'");
            ResultSet rs3 = connManager.queryDB("select * from AMBHOSPITAL where NUMREGISTRO= '" + numRegistro + "'");
            connManager.close();
            if (rs.next())
                if (rs2.next()) {
                    return new AmbPrivadaDTO(
                            numRegistro,
                            rs.getString("EQUIPO"),
                            rs.getDouble("LATITUD"),
                            rs.getDouble("LONGITUD"),
                            rs2.getString("COMPANYIA"));
                } else if (rs3.next()) {
                    return new AmbHospitalDTO(
                            numRegistro,
                            rs.getString("EQUIPO"),
                            rs.getDouble("LATITUD"),
                            rs.getDouble("LONGITUD"),
                            rs3.getString("IDHOSPITAL"));
                }
        } catch (SQLException e) {
            throw new ExcepcionDAO(e);
        }
        return null;
    }

}
