package emergencias.persistencia.dao.hsql;

import emergencias.excepciones.ExcepcionDAO;

import java.sql.*;

public class HSQLConnectionManager {
    private static final int HSQLDB_SERVER = 0; // hsqldb in server mode
    private static final int HSQLDB_STANDALONE = 1; // hsqldb in standalone mode
    // (embedded)
    private static final int HSQLDB_JAR = 2; // hsqldb in jar file (read-only)
    private static final int HSQLDB_MODE = HSQLDB_STANDALONE; // current mode
    private String sourceURL;
    private Connection dbcon = null;

    public HSQLConnectionManager(String dbname) throws ClassNotFoundException {
        Class.forName("org.hsqldb.jdbcDriver");
        switch (HSQLDB_MODE) {
            case HSQLDB_SERVER:
                sourceURL = "jdbc:hsqldb:hsql://localhost/" + dbname;
                break;
            case HSQLDB_STANDALONE:
                sourceURL = "jdbc:hsqldb:file:./data/" + dbname;
                break;
            case HSQLDB_JAR:
                sourceURL = "jdbc:hsqldb:res:/data/" + dbname;
        }

    }

    public void connect() throws ExcepcionDAO {
        if (dbcon == null)
            try {
                dbcon = DriverManager.getConnection(sourceURL);
            } catch (SQLException e) {
                throw new ExcepcionDAO("DB_CONNECT_ERROR");
            }
    }

    public void close() throws ExcepcionDAO {
        if (dbcon != null) {
            try {
                dbcon.close();
            } catch (SQLException e) {
                throw new ExcepcionDAO("DB_DISCONNECT_ERROR");
            }
            dbcon = null;
        }
    }

    public void updateDB(String sql) throws ExcepcionDAO {
        if (dbcon != null) {
            try {
                Statement sentencia = dbcon.createStatement();
                sentencia.executeUpdate(sql);
            } catch (SQLException e) {
                throw new ExcepcionDAO("DB_WRITE_ERROR");
            }
        }
    }

    public ResultSet queryDB(String sql) throws ExcepcionDAO {
        if (dbcon != null) {
            try {
                Statement sentencia = dbcon.createStatement();
                return sentencia.executeQuery(sql);
            } catch (SQLException e) {
                throw new ExcepcionDAO("DB_READ_ERROR");
            }
        }
        return null;
    }

    public void setAutoCommit(boolean autoCommit) throws SQLException {
        dbcon.setAutoCommit(autoCommit);
    }

    public void commit() throws SQLException {
        dbcon.commit();
    }

    public void rollback() throws SQLException {
        dbcon.rollback();
    }
}
