package emergencias.persistencia.dao.hsql;

import emergencias.excepciones.ExcepcionDAO;
import emergencias.persistencia.dao.PacienteDAO;
import emergencias.persistencia.dto.PacienteDTO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PacienteHSQLDAO implements PacienteDAO {
    protected HSQLConnectionManager connManager;

    public PacienteHSQLDAO(HSQLConnectionManager connManager) throws ExcepcionDAO {
        this.connManager = connManager;
    }

    @Override
    public void crearPaciente(PacienteDTO pa) throws ExcepcionDAO {
        try {
            connManager.connect();
            connManager
                    .updateDB("insert into PACIENTE (DNI, NOMBRE, APELLIDOS, DIRECCION, TELEFONO, EDAD, SEXO) values ('"
                            + pa.getDni() + "','" + pa.getNombre() + "','" + pa.getApellidos() + "', '"
                            + pa.getDireccion() + "', " + pa.getTelefono() + ", " + pa.getEdad() + ",'" + pa.getSexo()
                            + "')");

        } catch (Exception e) {
            throw new ExcepcionDAO("Error creando paciente en la BD");
        }
    }

    @Override
    public void borrarPaciente(String dni) throws ExcepcionDAO {
        if (buscarPaciente(dni) == null)
            try {
                connManager.connect();
                connManager
                        .updateDB("delete from PACIENTE where DNI= '" + dni + "'");
                connManager.close();
            } catch (Exception e) {
                throw new ExcepcionDAO("Error borrando paciente de la BD");
            }
    }

    @Override
    public List<PacienteDTO> listarPacientes() throws ExcepcionDAO {
        try {
            connManager.connect();
            ResultSet rs = connManager.queryDB("select * from PACIENTE");
            connManager.close();
            List<PacienteDTO> pacientes = new ArrayList<>();
            try {
                while (rs.next()) {
                    pacientes.add(buscarPaciente(rs.getString("DNI")));
                }
                return pacientes;
            } catch (SQLException e) {
                throw new ExcepcionDAO("Error listando pacientes de la BD");
            }
        } catch (ExcepcionDAO e) {
            throw e;
        }
    }

    @Override
    public PacienteDTO buscarPaciente(String dni) throws ExcepcionDAO {
        try {
            connManager.connect();
            ResultSet rs = connManager.queryDB("select * from PACIENTE where DNI= '" + dni + "'");
            connManager.close();
            if (rs.next()) {
                return new PacienteDTO(
                        dni,
                        rs.getString("NOMBRE"),
                        rs.getString("APELLIDOS"),
                        rs.getString("DIRECCION"),
                        rs.getInt("TELEFONO"),
                        rs.getInt("EDAD"),
                        rs.getString("SEXO").charAt(0));
            } else
                return null;
        } catch (SQLException e) {
            throw new ExcepcionDAO(e);
        }
    }
}
