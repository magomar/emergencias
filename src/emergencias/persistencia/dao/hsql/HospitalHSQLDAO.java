package emergencias.persistencia.dao.hsql;

import emergencias.excepciones.ExcepcionDAO;
import emergencias.persistencia.dao.HospitalDAO;
import emergencias.persistencia.dto.HospitalDTO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class HospitalHSQLDAO implements HospitalDAO {
    protected HSQLConnectionManager connManager;

    public HospitalHSQLDAO(HSQLConnectionManager connManager) throws ExcepcionDAO {
        this.connManager = connManager;
    }

    @Override
    public void crearHospital(HospitalDTO h) throws ExcepcionDAO {
        try {
            connManager.connect();
            connManager.updateDB("INSERT into HOSPITAL (NOMBRE, DIRECCION, LATITUD, LONGITUD) values ('" + h.getNombre()
                    + "','" + h.getDireccion() + "','" + h.getLatitud() + "', '" + h.getLongitud() + "')");
            connManager.close();
        } catch (Exception e) {
            throw new ExcepcionDAO(e);
        }
    }

    @Override
    public List<HospitalDTO> listarHospitales() throws ExcepcionDAO {
        try {
            connManager.connect();
            ResultSet rs = connManager.queryDB("SELECT * from HOSPITAL");
            connManager.close();
            List<HospitalDTO> hospitales = new ArrayList<>();
            try {
                while (rs.next()) {
                    hospitales.add(buscarHospital(rs.getString("NOMBRE")));
                }
                return hospitales;
            } catch (Exception e) {
                throw new ExcepcionDAO(e);
            }
        } catch (ExcepcionDAO e) {
            throw e;
        }
    }

    @Override
    public HospitalDTO buscarHospital(String nombre) throws ExcepcionDAO {
        try {
            connManager.connect();
            ResultSet rs = connManager.queryDB("SELECT * from HOSPITAL where NOMBRE= '" + nombre + "'");
            connManager.close();
            if (rs.next()) {
                return new HospitalDTO(
                        nombre,
                        rs.getString("DIRECCION"),
                        rs.getDouble("LATITUD"),
                        rs.getDouble("LONGITUD"));
            } else
                return null;
        } catch (SQLException e) {
            throw new ExcepcionDAO(e);
        }
    }

    @Override
    public List<HospitalDTO> listarHospitalesPorEspecialidad() throws ExcepcionDAO {
        // TODO
        return null;
    }

}
