package emergencias.persistencia.dao.hsql;

import emergencias.excepciones.ExcepcionDAO;
import emergencias.persistencia.dao.*;

/**
 * This class provides a Single Point of Access for DAOs backed by an HSQLDB storage
 */
public class HSQLDAOFactory implements DAOFactory {
    // Conexi�n a DB
    private HSQLConnectionManager connectionManager;
    // Declaraci�n de los DAO
    private PacienteDAO pacienteDAO;
    private HospitalDAO hospitalDAO;
    private AmbulanciaDAO ambulanciaDAO;
    private EmergenciaDAO emergenciaDAO;
    private EspecialidadDAO especialidadDAO;

    // constructor
    public HSQLDAOFactory(String dabaseName) {
        try {
            this.connectionManager = new HSQLConnectionManager(dabaseName);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public PacienteDAO damePacienteDAO() {
        if (pacienteDAO == null)
            try {
                pacienteDAO = new PacienteHSQLDAO(connectionManager);
            } catch (ExcepcionDAO excepcionDAO) {
                excepcionDAO.printStackTrace();
            }
        return pacienteDAO;
    }

    @Override
    public AmbulanciaDAO dameAmbulanciaDAO() {
        if (ambulanciaDAO == null)
            try {
                ambulanciaDAO = new AmbulanciaHSQLDAO(connectionManager);
            } catch (ExcepcionDAO excepcionDAO) {
                excepcionDAO.printStackTrace();
            }
        return ambulanciaDAO;
    }

    @Override
    public HospitalDAO dameHospitalDAO() {
        if (hospitalDAO == null)
            try {
                hospitalDAO = new HospitalHSQLDAO(connectionManager);
            } catch (ExcepcionDAO excepcionDAO) {
                excepcionDAO.printStackTrace();
            }
        return hospitalDAO;
    }

    @Override
    public EspecialidadDAO dameEspecialidadDAO() {
        if (especialidadDAO == null)
            try {
                especialidadDAO = new EspecialidadHSQLDAO(connectionManager);
            } catch (ExcepcionDAO excepcionDAO) {
                excepcionDAO.printStackTrace();
            }
        return especialidadDAO;
    }

    @Override
    public EmergenciaDAO dameEmergenciaDAO() {
        if (emergenciaDAO == null)
            try {
                emergenciaDAO = new EmergenciaHSQLDAO(connectionManager);
            } catch (ExcepcionDAO excepcionDAO) {
                excepcionDAO.printStackTrace();
            }
        return emergenciaDAO;
    }

//    private Object executeAndClose(DAOCommand command) throws ExcepcionLogica {
//        try {
//            return command.execute(this);
//        } finally {
//            try {
//                this.connectionManager.close();
//            } catch (ExcepcionDAO e) {
//                e.printStackTrace();
//            }
//        }
//    }
//
//    public Object transactionAndClose(DAOCommand command) throws ExcepcionLogica {
//        return executeAndClose(new DAOCommand() {
//            public Object execute(daoFactory manager) {
//                return manager.transaction(command);
//            }
//        });
//    }
//
//    public Object transaction(DAOCommand command) {
//        try {
//            connectionManager.setAutoCommit(false);
//            Object returnValue = command.execute(this);
//            connectionManager.commit();
//            return returnValue;
//        } catch (Exception e) {
//            try {
//                connectionManager.rollback();
//            } catch (SQLException e1) {
//                e1.printStackTrace();
//            }
//        } finally {
//            try {
//                this.connectionManager.setAutoCommit(true);
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//        }
//        return null;
//    }
}
