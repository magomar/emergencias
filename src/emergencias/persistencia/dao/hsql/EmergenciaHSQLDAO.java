package emergencias.persistencia.dao.hsql;

import emergencias.excepciones.ExcepcionDAO;
import emergencias.persistencia.dao.EmergenciaDAO;
import emergencias.persistencia.dto.EmergenciaDTO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EmergenciaHSQLDAO implements EmergenciaDAO {
    private HSQLConnectionManager connManager;

    public EmergenciaHSQLDAO(HSQLConnectionManager connManager) throws ExcepcionDAO {
        this.connManager = connManager;
    }

    @Override
    public void crearEmergencia(EmergenciaDTO emergencia) throws ExcepcionDAO {
        try {
            connManager.connect();
            connManager.updateDB(
                    "insert into EMERGENCIA (IDREGISTRO, LATITUD, LONGITUD, FECHA, HORA, DNIPACIENTE, IDAMBULANCIA, IDHOSPITAL, ESPECIALIDAD) values ('"
                            + emergencia.getIdEmergencia() + "','" + emergencia.getLatitud() + "','" + emergencia.getLongitud() + "', '"
                            + emergencia.getFecha() + "','" + emergencia.getHora() + "','" + emergencia.getDniPaciente() + "','"
                            + emergencia.getIdAmbulancia() + "','" + emergencia.getIdHospital() + "','" + emergencia.getIdEspecialidad() + "')");
            connManager.close();
        } catch (Exception e) {
            throw new ExcepcionDAO(e);
        }
    }

    @Override
    public List<EmergenciaDTO> listarEmergencias() throws ExcepcionDAO {
        try {
            connManager.connect();
            ResultSet rs = connManager.queryDB("select * from EMERGENCIA");
            connManager.close();
            List<EmergenciaDTO> emergencias = new ArrayList<>();
            try {
                while (rs.next()) {
                    emergencias.add(buscarEmergencia(rs.getString("IDEMERGENCIA")));
                }
                return emergencias;
            } catch (Exception e) {
                throw new ExcepcionDAO(e);
            }
        } catch (ExcepcionDAO e) {
            throw e;
        }
    }

    @Override
    public EmergenciaDTO buscarEmergencia(String idEmergencia) throws ExcepcionDAO {
        try {
            connManager.connect();
            ResultSet rs = connManager.queryDB("select * from EMERGENCIA where IDEMERGENCIA= '" + idEmergencia + "'");
            connManager.close();
            if (rs.next()) {
                return new EmergenciaDTO(
                        idEmergencia,
                        rs.getDouble("LATITUD"),
                        rs.getDouble("LONGITUD"),
                        rs.getString("FECHA"),
                        rs.getString("HORA"),
                        rs.getString("DNIPACIENTE"),
                        rs.getString("IDAMBULANCIA"),
                        rs.getString("IDHOSPITAL"),
                        rs.getString("ESPECIALIDAD"));
            } else
                return null;
        } catch (SQLException e) {
            throw new ExcepcionDAO(e);
        }
    }
}
