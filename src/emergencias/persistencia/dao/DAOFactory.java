package emergencias.persistencia.dao;

import emergencias.persistencia.dao.hsql.HSQLDAOFactory;
import emergencias.persistencia.dao.xml.XmlDAOFactory;

public interface DAOFactory {
    // List of DAO types supported by the factory
    int HSQLDB = 1;
    int XML = 2;

    static DAOFactory obtenerDAOFactory(int whichFactory) {
        switch (whichFactory) {
            case 1:
                return new HSQLDAOFactory("emergenciasBD");
            case 2:
                return new XmlDAOFactory("emergencias.xml");
            default:
                return null;
        }
    }


    HospitalDAO dameHospitalDAO();

    AmbulanciaDAO dameAmbulanciaDAO();

    PacienteDAO damePacienteDAO();

    EspecialidadDAO dameEspecialidadDAO();

    EmergenciaDAO dameEmergenciaDAO();
}

