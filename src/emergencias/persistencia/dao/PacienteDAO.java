package emergencias.persistencia.dao;

import emergencias.excepciones.ExcepcionDAO;
import emergencias.persistencia.dto.PacienteDTO;

import java.util.List;

public interface PacienteDAO {
    PacienteDTO buscarPaciente(String dni) throws ExcepcionDAO;

    void crearPaciente(PacienteDTO p) throws ExcepcionDAO;

    void borrarPaciente(String dni) throws ExcepcionDAO;

    List<PacienteDTO> listarPacientes() throws ExcepcionDAO;
}
