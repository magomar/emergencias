package emergencias.persistencia.dao;

import emergencias.excepciones.ExcepcionLogica;

public interface DAOCommand {

    Object execute(DAOFactory daoFactory) throws ExcepcionLogica;

}
