package emergencias.persistencia.dao;

import emergencias.excepciones.ExcepcionDAO;
import emergencias.persistencia.dto.HospitalDTO;

import java.util.List;

public interface HospitalDAO {
    HospitalDTO buscarHospital(String nombre) throws ExcepcionDAO;

    void crearHospital(HospitalDTO h) throws ExcepcionDAO;

    List<HospitalDTO> listarHospitales() throws ExcepcionDAO;

    List<HospitalDTO> listarHospitalesPorEspecialidad() throws ExcepcionDAO;
}
