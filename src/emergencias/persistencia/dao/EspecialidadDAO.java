package emergencias.persistencia.dao;

import emergencias.excepciones.ExcepcionDAO;
import emergencias.persistencia.dto.EspecialidadDTO;

import java.util.List;

public interface EspecialidadDAO {
    EspecialidadDTO buscarEspecialidad(String nombre) throws ExcepcionDAO;

    void crearEspecialidad(EspecialidadDTO especialidad) throws ExcepcionDAO;

    List<EspecialidadDTO> listarEspecialidades() throws ExcepcionDAO;

    List<EspecialidadDTO> listarEspecialidadesPorHospital(String nombreEspecialidad) throws ExcepcionDAO;
}
