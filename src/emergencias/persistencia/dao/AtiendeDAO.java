package emergencias.persistencia.dao;

import emergencias.excepciones.ExcepcionDAO;
import emergencias.persistencia.dto.AtiendeDTO;

import java.util.List;

/**
 * Created by Mario on 28/10/2015.
 */
public interface AtiendeDAO {

    void crearAtiende(AtiendeDTO especialidad) throws ExcepcionDAO;

    List<AtiendeDTO> listarAtiendeDTO() throws ExcepcionDAO;

}
