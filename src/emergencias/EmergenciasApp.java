package emergencias;

import emergencias.presentacion.control.ControladorPrincipal;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;

public class EmergenciasApp extends Application {
    private Stage primaryStage;
    private BorderPane rootLayout;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("SERVICIO DE EMERGENCIAS");

        // Set the application icon.
        Image appIcon = new Image(EmergenciasApp.class.getResourceAsStream("presentacion/vista/app-icon.png"));
        this.primaryStage.getIcons().add(appIcon);
        initRootLayout();
    }

    public void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(EmergenciasApp.class.getResource("presentacion/vista/principal.fxml"));
            rootLayout = (BorderPane) loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
            ControladorPrincipal controlador = loader.getController();
            controlador.setPrimaryStage(primaryStage);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
