package emergencias.presentacion.control;

import emergencias.logica.model.Paciente;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.net.URL;
import java.util.Collection;
import java.util.ResourceBundle;

public class ControladorListarPacientes extends ControladorDialogo {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TableView<Paciente> pacientes;

    @FXML
    private TableColumn<Paciente, String> apellidos;

    @FXML
    private TableColumn<Paciente, String> direccion;

    @FXML
    private TableColumn<Paciente, Integer> telefono;

    @FXML
    private TableColumn<Paciente, Character> sexo;

    @FXML
    private TableColumn<Paciente, String> nombre;

    @FXML
    private TableColumn<Paciente, Integer> edad;

    @FXML
    private TableColumn<Paciente, String> dni;

    @FXML
    private Button aceptar;

    @FXML
    void initialize() {

        dialog = new Stage(StageStyle.DECORATED);
        dialog.initModality(Modality.WINDOW_MODAL);
        setTitle("LISTAR PACIENTES");
        aceptar.setOnAction(event -> dialog.close());

        dni.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getDni()));
        nombre.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getNombre()));
        apellidos.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getApellidos()));
        direccion.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getDireccion()));
        sexo.setCellValueFactory(param -> new ReadOnlyObjectWrapper<Character>(param.getValue().getSexo()));
        // edad.setCellFactory(TextFieldTableCell.<PacienteDTO,
        // Integer>forTableColumn(new IntegerStringConverter()));
        edad.setCellValueFactory(param -> new ReadOnlyObjectWrapper<Integer>(param.getValue().getEdad()));
        telefono.setCellValueFactory(param -> new ReadOnlyObjectWrapper<Integer>(param.getValue().getTelefono()));
    }

    public ControladorListarPacientes setPacientes(Collection<Paciente> pacientes) {
        this.pacientes.getItems().clear();
        this.pacientes.getItems().addAll(pacientes);
        return this;
    }


}
