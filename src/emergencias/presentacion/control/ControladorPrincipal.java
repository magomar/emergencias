package emergencias.presentacion.control;

import emergencias.excepciones.ExcepcionLogica;
import emergencias.logica.model.Paciente;
import emergencias.logica.service.ServicioEmergencias;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.stage.Stage;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class ControladorPrincipal {
    private static final Logger LOG = Logger.getLogger(ControladorPrincipal.class.getName());
    private static final String ALTA_PACIENTE = "../vista/alta-paciente.fxml";
    private static final String LISTAR_PACIENTES = "../vista/listar-pacientes.fxml";
    private ServicioEmergencias servicioEmergencias;
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    private Stage primaryStage;

    @FXML
    void initialize() {
        servicioEmergencias = ServicioEmergencias.dameServicioEmergencias();
    }

    @FXML
    void altaPaciente(ActionEvent event) throws ExcepcionLogica {
        Paciente paciente = initDialogo(ALTA_PACIENTE, ControladorAltaPaciente.class).getNuevoPaciente();
        if (paciente != null)
            servicioEmergencias.altaPaciente(paciente);
        LOG.log(Level.INFO, "PacienteDTO dado de alta: " + paciente);
    }

    @FXML
    void consultarPaciente(ActionEvent event) {
        LOG.log(Level.INFO, "Consultar paciente NO IMPLEMENTADO");

    }

    @FXML
    void listarPacientes(ActionEvent event) throws ExcepcionLogica {
        List<Paciente> pacientes = servicioEmergencias.listarPacientes();
        initDialogo(LISTAR_PACIENTES, ControladorListarPacientes.class).setPacientes(pacientes).show();
        LOG.log(Level.INFO, "Listado de pacientes: " + pacientes);
    }

    @FXML
    void salir(ActionEvent event) {
        Platform.exit();
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;

    }

    private <T extends ControladorDialogo> T initDialogo(String urlVista, Class<T> controlClass) {
        return ControladorDialogo.initDialogo(urlVista, controlClass, primaryStage);
    }

//    private ControladorAltaPaciente initAltaPaciente() {
//        FXMLLoader fxmlLoader = new FXMLLoader(
//                ControladorAltaPaciente.class.getResource("../vista/alta-paciente.fxml"));
//        ControladorAltaPaciente controlador = null;
//        try {
//            Parent parent = fxmlLoader.load();
//            controlador = fxmlLoader.getController();
//            controlador.setRoot(parent);
//            controlador.setOwner(primaryStage);
//        } catch (NullPointerException | IOException e) {
//            e.printStackTrace();
//        }
//        return controlador;
//    }
//
//    private ControladorListarPacientes initListarPacientes() {
//        FXMLLoader fxmlLoader = new FXMLLoader(
//                ControladorListarPacientes.class.getResource("../vista/listar-pacientes.fxml"));
//        ControladorListarPacientes controlador = null;
//        try {
//            Parent parent = fxmlLoader.load();
//            controlador = fxmlLoader.getController();
//            controlador.setRoot(parent);
//            controlador.setOwner(primaryStage);
//        } catch (NullPointerException | IOException e) {
//            e.printStackTrace();
//        }
//        return controlador;
//    }

}
