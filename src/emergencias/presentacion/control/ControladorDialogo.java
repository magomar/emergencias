package emergencias.presentacion.control;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by Mario on 01/11/2015.
 */
public abstract class ControladorDialogo {
    protected Stage dialog;
    protected Parent root;

    public static <T extends ControladorDialogo> T initDialogo(String urlVista, Class<T> controlClass, Stage owner) {
        FXMLLoader fxmlLoader = new FXMLLoader(ControladorDialogo.class.getResource(urlVista));
        T controlador = null;
        try {
            Parent parent = fxmlLoader.load();
            controlador = fxmlLoader.getController();
            controlador.setRoot(parent);
            controlador.setOwner(owner);
        } catch (NullPointerException | IOException e) {
            e.printStackTrace();
        }
        return controlador;
    }

    public void setTitle(String title) {
        dialog.setTitle(title);
    }

    public void setOwner(Stage parent) {
        dialog.initOwner(parent);
    }

    public void setRoot(Parent root) {
        this.root = root;
    }

    public void showAndWait() {
        dialog.setScene(new Scene(root));
        dialog.showAndWait();
    }

    public void show() {
        dialog.setScene(new Scene(root));
        dialog.show();
    }


}
