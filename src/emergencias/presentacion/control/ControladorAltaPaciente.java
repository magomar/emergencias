package emergencias.presentacion.control;

import emergencias.logica.model.Paciente;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.net.URL;
import java.util.ResourceBundle;

public class ControladorAltaPaciente extends ControladorDialogo {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField apellidos;

    @FXML
    private Button cancelar;

    @FXML
    private TextField direccion;

    @FXML
    private RadioButton hombre;

    @FXML
    private Button aceptar;

    @FXML
    private RadioButton mujer;

    @FXML
    private TextField telefono;

    @FXML
    private ToggleGroup sexo;

    @FXML
    private TextField nombre;

    @FXML
    private TextField edad;

    @FXML
    private TextField dni;

    private Paciente nuevoPaciente;

    @FXML
    void initialize() {
        dialog = new Stage(StageStyle.DECORATED);
        dialog.initModality(Modality.WINDOW_MODAL);
        setTitle("ALTA PACIENTE");
        cancelar.setOnAction(event -> dialog.close());
        aceptar.setOnAction(event -> {
            nuevoPaciente = new Paciente(dni.getText(), nombre.getText(), apellidos.getText(), direccion.getText(),
                    Integer.parseInt(telefono.getText()), Integer.parseInt(edad.getText()),
                    (hombre.isSelected() ? 'H' : 'M'));
            dialog.close();
        });
    }

    public Paciente getNuevoPaciente() {
        showAndWait();
        return nuevoPaciente;
    }


}
