package emergencias.logica.model;

import java.util.List;

public class Hospital {

    private String nombre;
    private String direccion;
    private Coordenadas coordenadas;
    /**
     * Relacion con {@link Especialidad} con multiplicidad 1..n
     */
    private List<Especialidad> especialidades;
    /**
     * Relacion con {@link Emergencia} con multiplicidad 0..n
     */
    private List<Emergencia> emergencias;
    /**
     * Relacion con {@link AmbHospital}, con multiplicidad 0..n
     * Son las ambulancias con base en este hospital
     */
    private List<AmbHospital> ambulancias;

    public Hospital(String nombre, String direccion, Coordenadas coordenadas) {
        super();
        this.nombre = nombre;
        this.direccion = direccion;
        this.coordenadas = coordenadas;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Coordenadas getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(Coordenadas coordenadas) {
        this.coordenadas = coordenadas;
    }

    public List<Especialidad> getEspecialidades() {
        return especialidades;
    }

    public void setEspecialidades(List<Especialidad> especialidades) {
        this.especialidades = especialidades;
    }

    public List<Emergencia> getEmergencias() {
        return emergencias;
    }

    public void setEmergencias(List<Emergencia> emergencias) {
        this.emergencias = emergencias;
    }

    public List<AmbHospital> getAmbulancias() {
        return ambulancias;
    }

    public void setAmbulancias(List<AmbHospital> ambulancias) {
        this.ambulancias = ambulancias;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Hospital hospital = (Hospital) o;

        return nombre.equals(hospital.nombre);

    }

    @Override
    public int hashCode() {
        return nombre.hashCode();
    }


}
