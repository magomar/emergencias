package emergencias.logica.model;

public class AmbHospital extends Ambulancia {
    /**
     * Relaci�n con {@link Hospital} con multiplicidad 1
     */
    private Hospital hospital;

    public AmbHospital(String numRegistro, String equipo, double latitud, double longitud, Hospital hospital) {
        super(numRegistro, equipo, latitud, longitud);
        this.hospital = hospital;
    }

    public Hospital getHospital() {
        return hospital;
    }

    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
    }

    @Override
    public String toString() {
        return "AmbHospital{" +
                "hospital=" + hospital +
                '}';
    }
}
