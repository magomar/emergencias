package emergencias.logica.model;

/**
 * Created by Mario on 24/11/2015.
 */
public class Coordenadas {
    private double latitud;
    private double longitud;

    public double getLatitud() {

        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

}
