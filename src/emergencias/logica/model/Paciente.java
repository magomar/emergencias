package emergencias.logica.model;

public class Paciente {
    private String dni;
    private String nombre;
//    private String apellidos;
//    private String direccion;
//    private int telefono;
    private int edad;
    private char sexo;

    public Paciente(String dni, String nombre, String apellidos, String direccion, int telefono, int edad, char sexo) {
        this.dni = dni;
        this.nombre = nombre;
//        this.apellidos = apellidos;
//        this.direccion = direccion;
//        this.telefono = telefono;
        this.edad = edad;
        this.sexo = sexo;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
//
//    public String getApellidos() {
//        return apellidos;
//    }
//
//    public void setApellidos(String apellidos) {
//        this.apellidos = apellidos;
//    }
//
//    public String getDireccion() {
//        return direccion;
//    }
//
//    public void setDireccion(String direccion) {
//        this.direccion = direccion;
//    }
//
//    public int getTelefono() {
//        return telefono;
//    }
//
//    public void setTelefono(int telefono) {
//        this.telefono = telefono;
//    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    @Override
    public String toString() {
        return "Paciente{" +
                "dni='" + dni + '\'' +
                ", nombre='" + nombre + '\'' +
//                ", apellidos='" + apellidos + '\'' +
//                ", direccion='" + direccion + '\'' +
//                ", telefono=" + telefono +
                ", edad=" + edad +
                ", sexo=" + sexo +
                '}';
    }

}
