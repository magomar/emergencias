package emergencias.logica.model;

public class AmbPrivada extends Ambulancia {

    private String companyia;

    public AmbPrivada(String numRegistro, String equipo, double latitud, double longitud, String companyia) {
        super(numRegistro, equipo, latitud, longitud);
        this.companyia = companyia;
    }

    public String getCompanyia() {
        return companyia;
    }

    public void setCompanyia(String companyia) {
        this.companyia = companyia;
    }

    @Override
    public String toString() {
        return "AmbPrivada{" +
                "companyia='" + companyia + '\'' +
                '}';
    }
}