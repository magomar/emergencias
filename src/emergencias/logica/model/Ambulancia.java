package emergencias.logica.model;

public abstract class Ambulancia {
    private String numRegistro;
    private String equipo;
    private Coordenadas coordenadas;
    private boolean disponibilidad;
    private Emergencia emergencia;

    public Ambulancia(String numRegistro, String equipo, double latitud, double longitud) {
        this.numRegistro = numRegistro;
        this.equipo = equipo;

        this.disponibilidad = true;
    }

    public String getNumRegistro() {
        return numRegistro;
    }

    public void setNumRegistro(String numRegistro) {
        this.numRegistro = numRegistro;
    }

    public String getEquipo() {
        return equipo;
    }

    public void setEquipo(String equipo) {
        this.equipo = equipo;
    }

    public Coordenadas getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(Coordenadas coordenadas) {
        this.coordenadas = coordenadas;
    }

    public boolean isDisponibilidad() {
        return disponibilidad;
    }

    public void setDisponibilidad(boolean disponibilidad) {
        this.disponibilidad = disponibilidad;
    }

    public Emergencia getEmergencia() {
        return emergencia;
    }

    public void setEmergencia(Emergencia emergencia) {
        this.emergencia = emergencia;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ambulancia that = (Ambulancia) o;

        return numRegistro.equals(that.numRegistro);

    }

    @Override
    public int hashCode() {
        return numRegistro.hashCode();
    }


}
