package emergencias.logica.model;

import java.time.LocalDateTime;

public class Emergencia {
    private String idEmergencia;
    private Coordenadas coordenadas;
    private LocalDateTime fechaHora;
    private Ambulancia ambAsignada;
    private Paciente paciente;
    private Hospital hospital;
    private Especialidad especialidad;

    public Emergencia(String idEmergencia, Coordenadas coordenadas, LocalDateTime fechaHora) {
        this.idEmergencia = idEmergencia;
        this.coordenadas = coordenadas;
        this.fechaHora = fechaHora;
    }

    public String getIdEmergencia() {
        return idEmergencia;
    }

    public void setIdEmergencia(String idEmergencia) {
        this.idEmergencia = idEmergencia;
    }

    public Coordenadas getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(Coordenadas coordenadas) {
        this.coordenadas = coordenadas;
    }

    public LocalDateTime getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(LocalDateTime fechaHora) {
        this.fechaHora = fechaHora;
    }

    public Ambulancia getAmbAsignada() {
        return ambAsignada;
    }

    public void setAmbAsignada(Ambulancia ambAsignada) {
        this.ambAsignada = ambAsignada;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Hospital getHospital() {
        return hospital;
    }

    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
    }

    public Especialidad getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(Especialidad especialidad) {
        this.especialidad = especialidad;
    }

}
