package emergencias.logica.model;

import java.util.List;

public class Especialidad {

    private String nombre;
    /**
     * Relacion con Hospital (0..n)
     */
    private List<Hospital> hospitales;

    public Especialidad(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Hospital> getHospitales() {
        return hospitales;
    }

    public void setHospitales(List<Hospital> hospitales) {
        this.hospitales = hospitales;
    }

    @Override
    public String toString() {
        return "Especialidad{" +
                "nombre='" + nombre + '\'' +
                ", hospitales=" + hospitales +
                '}';
    }
}
