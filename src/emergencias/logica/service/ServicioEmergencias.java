package emergencias.logica.service;

import emergencias.excepciones.ExcepcionDAO;
import emergencias.excepciones.ExcepcionLogica;
import emergencias.logica.model.*;
import emergencias.persistencia.dao.AmbulanciaDAO;
import emergencias.persistencia.dao.DAOFactory;
import emergencias.persistencia.dao.HospitalDAO;
import emergencias.persistencia.dao.PacienteDAO;
import emergencias.persistencia.dto.AmbHospitalDTO;
import emergencias.persistencia.dto.AmbPrivadaDTO;
import emergencias.persistencia.dto.HospitalDTO;
import emergencias.persistencia.dto.PacienteDTO;

import java.util.*;
import java.util.stream.Collectors;

public final class ServicioEmergencias {
    // Única instancia de la clase
    private static ServicioEmergencias INSTANCIA = new ServicioEmergencias();
    private DAOFactory daoFactory = DAOFactory.obtenerDAOFactory(DAOFactory.HSQLDB);
    private Map<String, Paciente> pacientes = new HashMap<>();
    private List<Emergencia> emergencias = new ArrayList<>();
    private Set<Hospital> hospitales = new HashSet<>();
    private Set<Ambulancia> ambulancias = new HashSet<>();

    // Constructor privado para impedir la creación de nuevos objetos
    private ServicioEmergencias() {
        cargaSistema();
    }

    // Método para obtener la instancia de la clase
    public static ServicioEmergencias dameServicioEmergencias() {
        return INSTANCIA;
    }

    private void cargaSistema() {
        // Se encargara de cargar objetos de la BD que no cambian
        // De momento como no tenemos BD podemos crearlos manualmente
//        listarHospitales();
//        listarAmbulancias();

    }

    /**
     * Caso de uso Dar de Alta Paciente. Añade un nuevo paciente a la colección de pacientes
     *
     * @param paciente
     * @throws ExcepcionLogica si el paciente ya existe
     */
    public void altaPaciente(Paciente paciente) throws ExcepcionLogica {
        PacienteDAO dao = daoFactory.damePacienteDAO();
        try {
            if (dao.buscarPaciente(paciente.getDni()) == null) {
                pacientes.put(paciente.getDni(), paciente);
                dao.crearPaciente(new PacienteDTO(
                        paciente.getDni(),
                        paciente.getNombre(),
                        paciente.getApellidos(),
                        paciente.getDireccion(),
                        paciente.getTelefono(),
                        paciente.getEdad(), paciente.getSexo()));
            } else
                throw new ExcepcionLogica("El paciente ya existe.");
        } catch (ExcepcionDAO e) {
            e.printStackTrace();
        }
    }

    /**
     * Caso de uso Buscar Paciente. Busca y recupera un paciente con el {@code dni} pasado como parámetro
     *
     * @param dni
     * @return
     */
    public Paciente buscarPaciente(String dni) {
        // Se busca el paciente en memoria
        Paciente paciente = pacientes.get(dni);
        // Si el paciente no esta en memoria, se busca en la BD
        if (paciente == null)
            try {
                PacienteDTO p = daoFactory.damePacienteDAO().buscarPaciente(dni);
                if (p != null) {
                    paciente = new Paciente(p.getDni(), p.getNombre(), p.getApellidos(),
                            p.getDireccion(), p.getTelefono(), p.getEdad(), p.getSexo());
                }
            } catch (ExcepcionDAO excepcionDAO) {
                excepcionDAO.printStackTrace();
            }
        return paciente;
    }

    public List<Paciente> listarPacientes() {
        List<Paciente> pacientes = null;
        try {
            // Se obtienen los pacientes de la BD, porque no hay ninguna garantía de tenerlos todos en memoria
            pacientes = daoFactory.damePacienteDAO().listarPacientes().stream().map(p -> new Paciente(
                    p.getDni(), p.getNombre(), p.getApellidos(), p.getDireccion(), p.getTelefono(), p.getEdad(), p.getSexo()
            )).collect(Collectors.toList());
            // se guardan los pacientes en memoria
            pacientes.stream().map(p -> this.pacientes.put(p.getDni(), p));
        } catch (ExcepcionDAO excepcionDAO) {
            excepcionDAO.printStackTrace();
        }
        return pacientes;
    }

    public void eliminarPaciente(Paciente paciente) throws ExcepcionLogica {
        eliminarPaciente(paciente.getDni());
    }

    public void eliminarPaciente(String dni) throws ExcepcionLogica {
        if (buscarPaciente(dni) == null) throw new ExcepcionLogica("El paciente con DNI " + dni + " no existe");
        pacientes.remove(dni);
        try {
            daoFactory.damePacienteDAO().borrarPaciente(dni);
        } catch (ExcepcionDAO excepcionDAO) {
            excepcionDAO.printStackTrace();
        }
    }

    public void anyadirHospital(Hospital hospital) throws ExcepcionLogica {
        try {
            HospitalDAO dao = daoFactory.dameHospitalDAO();
            if (dao.buscarHospital(hospital.getNombre()) == null) {
                hospitales.add(hospital);
                dao.crearHospital(new HospitalDTO(
                        hospital.getNombre(),
                        hospital.getDireccion(),
                        hospital.getLatitud(),
                        hospital.getLongitud()));
            } else
                throw new ExcepcionLogica("El hospital " + hospital.getNombre() + " ya existe.");
        } catch (ExcepcionDAO e) {
            e.printStackTrace();
        }
    }

    public List<Hospital> listarHospitales() {
        List<Hospital> hospitales = null;
        try {
            // Se obtienen los hospitales de la BD, porque no hay ninguna garantía de tenerlos todos en memoria
            hospitales = daoFactory.dameHospitalDAO().listarHospitales().stream().map(h -> new Hospital(
                    h.getNombre(), h.getDireccion(), h.getLatitud(), h.getLongitud()
            )).collect(Collectors.toList());
            hospitales.stream().map(h -> this.hospitales.add(h));
        } catch (ExcepcionDAO excepcionDAO) {
            excepcionDAO.printStackTrace();
        }
        return hospitales;
    }

    public void anyadirAmbulancia(AmbPrivada ambulancia) throws ExcepcionLogica {
        try {
            AmbulanciaDAO dao = daoFactory.dameAmbulanciaDAO();
            if (dao.buscarAmbulancia(ambulancia.getNumRegistro()) == null) {
                ambulancias.add(ambulancia);
                dao.crearAmbulancia(new AmbPrivadaDTO(
                        ambulancia.getNumRegistro(),
                        ambulancia.getEquipo(),
                        ambulancia.getLatitud(),
                        ambulancia.getLongitud(),
                        ambulancia.getCompanyia()
                ));
            } else
                throw new ExcepcionLogica("La ambulancia " + ambulancia.getNumRegistro() + " ya existe.");
        } catch (ExcepcionDAO e) {
            e.printStackTrace();
        }
    }

    public void anyadirAmbulancia(AmbHospital ambulancia) throws ExcepcionLogica {
        try {
            AmbulanciaDAO dao = daoFactory.dameAmbulanciaDAO();
            if (dao.buscarAmbulancia(ambulancia.getNumRegistro()) == null) {
                ambulancias.add(ambulancia);
                dao.crearAmbulancia(new AmbHospitalDTO(
                        ambulancia.getNumRegistro(),
                        ambulancia.getEquipo(),
                        ambulancia.getLatitud(),
                        ambulancia.getLongitud(),
                        ambulancia.getHospital().getNombre()
                ));
            } else
                throw new ExcepcionLogica("La ambulancia " + ambulancia.getNumRegistro() + " ya existe.");
        } catch (ExcepcionDAO e) {
            e.printStackTrace();
        }
    }

    public List<Ambulancia> listarAmbulancias() {
        List<Ambulancia> ambulancias = null;
        try {
            // Se obtienen los ambulancias de la BD, porque no hay ninguna garant�a de tenerlos todos en memoria
            ambulancias = daoFactory.dameAmbulanciaDAO().listarAmbulancias().stream().map(a -> {
                        if (a instanceof AmbPrivadaDTO)
                            return new AmbPrivada(
                                    a.getNumRegistro(), a.getEquipo(), a.getLatitud(), a.getLongitud(), ((AmbPrivadaDTO) a).getCompanyia());
                        else {
                            String nomHospital = ((AmbHospitalDTO) a).getIdHospital();
                            Hospital h = buscarHospital(nomHospital);
                            return new AmbHospital(
                                    a.getNumRegistro(), a.getEquipo(), a.getLatitud(), a.getLongitud(), h);
                        }
                    }
            ).collect(Collectors.toList());
            ambulancias.stream().map(a -> this.ambulancias.add(a));
        } catch (ExcepcionDAO excepcionDAO) {
            excepcionDAO.printStackTrace();
        }
        return ambulancias;
    }

    public Hospital buscarHospital(String nomHospital) {
        // Se busca el hospital en memoria
        Optional<Hospital> optHospital = hospitales.stream()
                .filter(h -> h.getNombre().equals(nomHospital)).findFirst();
        if (optHospital.isPresent()) return optHospital.get();
        // Si el hospital no esta en memoria, se busca en la BD
        Hospital hospital = null;
        try {
            HospitalDTO h = daoFactory.dameHospitalDAO().buscarHospital(nomHospital);
            if (h != null) {
                hospital = new Hospital(
                        h.getNombre(),
                        h.getDireccion(),
                        h.getLatitud(),
                        h.getLongitud());
            }
        } catch (ExcepcionDAO excepcionDAO) {
            excepcionDAO.printStackTrace();
        }
        return hospital;
    }

}
