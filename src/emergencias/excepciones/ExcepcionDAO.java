package emergencias.excepciones;

@SuppressWarnings("serial")
public class ExcepcionDAO extends Exception {

    public ExcepcionDAO(String message) {
        super(message);
    }

    public ExcepcionDAO(Exception e) {
        super(e.getMessage());
    }

}
