package emergencias.excepciones;

@SuppressWarnings("serial")
public class ExcepcionLogica extends Exception {

    public ExcepcionLogica(String message) {
        super(message);
    }

    public ExcepcionLogica(Exception e) {
        super(e.getMessage());
    }

}
